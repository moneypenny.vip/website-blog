---
layout: page
permalink: /jobs/
---

# Jobs

If you're interested in working at ViperDev shoot us an email to
[jobs@viperdev.io](mailto:jobs@viperdev.io) with some information about you.

Be sure to phrase the mail in english. You will be judged by an internationally
distributed team.

Here's a couple of questions you may want to answer:

- What is your motivation for working at ViperDev?
- What would be your ideal job?
    - Where is it?
    - What do you do there?
    - Full-time, part-time, intern, contractor?
- What do you think we lack as a company?
- Do you want to eventually start founding your own company? Anything in the
  works?
